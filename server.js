const express = require('express'),
     bodyParser = require('body-parser'),
     MongoServer = require('./config/db'),
     user_route = require("./routes/user.routes"),
     passport = require('passport'),
     config = require('./config/env.config'),
     app = express(),
     handleError = require('./middleware/error').handleError;
//Initiate mongo Server
MongoServer(); 

//Port
 PORT = process.env.PORT || 8040


 
  
app.use(passport.initialize()); 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(cors());

//Call method Signup user
app.use('/api/wallet',user_route);

app.get('/error', (req, res) => {
    throw new ErrorHandler(500, 'Internal server error');
  })
  //Handler Error
  app.use((err, req, res, next) => {
      handleError(err, res);
    });
  
  //Start Server
  app.listen(config.port, (req,res)=>{
      console.log('Server start at Port '+config.port);
  })


module.exports=app;