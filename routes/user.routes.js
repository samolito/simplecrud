 
﻿const express = require('express');
const router = express.Router();
const userController = require('../controller/user.controller');
const auth = require ('../middleware/auth');
const passport = require('passport');
const keys = require('../config/keys')
const helpers = require('../middleware/helpers')
const githubOAuth = require('github-oauth')({
  githubClient: keys.github.clientID,
  githubSecret: keys.github.clientSecret,
  baseURL: 'http://localhost:4040/api',
  loginURI: '/auth/github',
  callbackURI: '/auth/github/callback',
  scope:'user'
})

//auth(passport);

//register route function
router.post('/register', 
//helpers.validFieldsRegister,
userController.register);

router.post('/create', 
userController.createAccount);

router.post('/newaccount', 
userController.createNewaccount);

router.get('/allusers', 
userController.getall);

router.get('/allaccounts', 
userController.getaccounts);

router.put('/updateuser', 
userController.UpdateUser);

router.put('/updateaccount', 
userController.UpdateAccount);

router.post('/delete', 
userController.deleteAccount);

router.get('/getme', 
auth.validJwt,
userController.getme);

//retrieve  user by email
router.get('/getuser/:email',
userController.getuser);
// //logout user router
// router.post('/logout',userController.logout); 

// //retrieve logged user
// router.get('/user',auth,userController.getuser);

// //auth with google
// router.get('/auth/google', passport.authenticate('google',{
// scope:['profile']
// }));

// //Callback with google
// router.get('/auth/google/callback',passport.authenticate('google'),
//  (req,res)=>{
//     res.send('You reach the callback URI');
//   });



module.exports = router;