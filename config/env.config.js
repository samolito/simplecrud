module.exports = {
    "port": 8040,
    "appEndpoint": "http://localhost:4040",
    "apiEndpoint": "http://localhost:40400",
    "jwt_secret": "wallet1234",
    "jwt_expiration": 3600,
    "environment": "dev",
    "permissionLevels": {
        "NORMAL_USER": 1,
        //"PAID_USER": 4,
        "ADMIN": 2048
    }
};