var randomize = require('randomatic');
const bycrypt = require('bcryptjs');
const jwt = require ('jsonwebtoken');
jwtSecret = require('../config/env.config').jwt_secret
const User = require('../model/user');
const Account = require('../model/account');
 let  { handleError, ErrorHandler}  = require('../middleware/error');
// const emailconfig= require('../middleware/emailconfig');
// var randomize = require('randomatic');
// var DateDiff = require('date-diff');
  0
let newUser;
let code;
let pwd;



//Get user information for registration function
const register =  async (req, res, next)=>{
  try {
   if (await User.findOne({ email: req.body.email })) { 
        throw new ErrorHandler(401,'User already exists',null);
     }
      newUser = new User(req.body);
      pwd = req.body.password;
      code=randomize('0', 6);
        res.status(200).json({ 
          status:1, 
          message:"Verification code sent",
          code:code
        });
    next()
  } catch (error) {
    next(error)
  }
};

//Verify code and create Account 
const createAccount =  async(req, res, next)=>{
  try {
    //Verify Code
   let receivedCode = parseInt(req.body.code);
  if(code!=receivedCode){
    throw new ErrorHandler(401,'Code Incorrect',60);
  }
  
  //*********Create Account********
  // //Generate Salt
  const salt = await bycrypt.genSalt(10);
  //Hash user password
  newUser.password = await bycrypt.hash(newUser.password,salt);
 // newUser.accountNumber=randomize('0', 10);
  //newUser.balance =0.0
  //newUser.created_at =Date.now();
  //newUser.lastUpdated =Date.now();
  //save user and generate token
  
 //Create account 
//  newUser.accounts.push({
//      acn:randomize('0', 10),
//      balance:0.0,
//      currency:'VND'
//  });
//  newUser.accounts.acn=randomize('0', 10);
//  newUser.accounts.balance=0.0;
//  newUser.accounts.currency='VND';
 let account = new Account({
  acn:randomize('0', 10),
  balance:0.0,
  currency:'VND',
  user:newUser
 })
 //newUser.save();
 account.save();
 newUser.accounts.push(account)
 newUser.save()
 //const usersaved=await account.save();
 //await usersaved.populate('user').execPopulate()

 //newUser.accounts.push(account);
  const payload = { newUser: { id: newUser.id, email:newUser.email,permissionLevel:newUser.permissionLevel} };
    jwt.sign(payload,jwtSecret,{ expiresIn: 60 * 60 }, 
      (err,token)=>{
        if(err)throw err, next();
        res.status(200).json({ 
          status:1,
          token
         });
      });   
    next()
  } catch (error) {
    next(error)
  }
};
//add another account 
const createNewaccount =  async (req, res, next)=>{
  try {
   const user =await User.findOne({ email: req.body.email })
   if (!user) { 
        throw new ErrorHandler(401,'User bot found',null);
     }
     let account = new Account({
      acn:randomize('0', 10),
      balance:0.0,
      currency:'VND',
      user:user
     })
     account.save()
     user.accounts.push(account)
     user.save()
        res.status(200).json({ 
          status:1, 
          message:"Account created",
          user
        });
    next()
  } catch (error) {
    next(error)
  }
};


//return all user 
const getall =  async (req, res, next)=>{
  try {
   users = await User.find().populate('accounts');
  return res.status(200).json({
    status:1,
    results: users,
  });
} catch (error) {
    next(error);
}
};
//get user by email
const getuser= async(req,res,next)=>{
  try {
    const user = await User.findOne({email:req.params.email}).populate('accounts')
    if(user){
     res.status(201).json({status:1,user:user});
   }
  else{
    throw new ErrorHandler(401,'User Not Exist',null);
  }
  } catch (error) {
   next(error)
  }

};
//return all user 
const getaccounts =  async (req, res, next)=>{
  try {
   accounts = await Account.find().populate('user');
  return res.status(200).json({
    status:1,
    results: accounts,
  });
} catch (error) {
    next(error);
}
};
const UpdateUser= async(req,res,next)=>{
  const user = await User.findOne({email:req.body.email});
  try { 
  if(user){
    user.fullName=req.body.fullName
    user.phone=req.body.phone
    user.lastUpdated= Date.now();
    user.save();
   //return new handleError(200,'User removed');
    res.status(200).json({
      message:'User updated',
      status:1,
      user});
    }
  else{
    throw new ErrorHandler(401,'User not exits',null);
  }
    next()
    } catch(error) {  
      next(error)
  }
};
const UpdateAccount= async(req,res,next)=>{
  const account = await Account.findOne({acn:req.body.acn});
  try { 
  if(account){
    account.balance=req.body.balance
    account.save();
   //return new handleError(200,'User removed');
    res.status(200).json({
      message:'Account updated',
      status:1,
      account});
    }
  else{
    throw new ErrorHandler(401,'Account not exits',null);
  }
    next()
    } catch(error) {  
      next(error)
  }
};
////////////////////
//Delete user function
const deleteAccount= async(req,res,next)=>{
  const user = await User.findOne({email:req.body.email});
  try { 
  if(user){
    user.status = false
    user.lastUpdated= Date.now();
    user.save();
   //return new handleError(200,'User removed');
    res.status(200).json({token:null,
      message:'User removed',
      status:1});
    }
  else{
    throw new ErrorHandler(401,'User not exits',null);
  }
    next()
    } catch(error) {  
      next(error)
  }
};

const getme= async (req, res,next ) => {
 // try {
    // request.user is getting fetched from Middleware after token authentication
    //const user = await User.findById(req.user.id);
    res.status(200).json({
      statut:1,
      user:req.user});
    next()
  // } catch (e) {
  //   next(e)
  //   //res.send({ message: "Error in Fetching user" });
  // }
};
module.exports = {register, 
  getall, createAccount, 
  createNewaccount,
  deleteAccount, getme,getuser,
  getaccounts,UpdateUser,UpdateAccount};
                     