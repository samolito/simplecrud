const validator = require('validator')
const User = require('../model/user');
let  { handleError, ErrorHandler}  = require('./error');

const validFieldsRegister = (req, res, next) => {
    // let errors = [];
      if (req.body) {
          if(validator.isEmpty(req.body.fullName)){
           throw new ErrorHandler(401,'Missing Name field',null);
          }
          if(validator.isEmpty(req.body.email)){
           throw new ErrorHandler(401,'Missing email field',null);
          }
          if(validator.isEmpty(req.body.password)){
           throw new ErrorHandler(401,'Missing password field',null);
          }
          if(validator.isEmpty(req.body.phone)){
            throw new ErrorHandler(401,'Missing phone field',null);
           }
          if(!validator.isEmail(req.body.email)){
           throw new ErrorHandler(401,'Incorrect email',null);
          }
           else {
            return next();
        }      
        }else{
            throw new ErrorHandler(500,'Server not found',null);
        }
  };
module.exports={validFieldsRegister}