const passport = require('passport');
const User = require('../model/user');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const GithubStrategy = require('passport-github').Strategy;

const keys = require('../config/keys')
 
passport.serializeUser((user, done) => {
    done(null, user.id);
  });
  
  passport.deserializeUser((id, done) => {
    User.findById(id)
      .then(user => {
        done(null, user);
      })
  });

passport.use(
   new GoogleStrategy({
//Option for google strat
       clientID:keys.google.clientID,
       clientSecret:keys.google.clientSecret,
       callbackURL:'/api/auth/google/callback',
   },async(accessToken, refreshToken, profile, cb)=> {
    const existingUser =  await User.findOne({email: profile.emails[0].value});
         if(existingUser){
             return console.log('User already exist');
         }else{
         const user = await new User({
              email:profile.emails[0].value,
              username:profile.displayName,
              password:profile.id,
              confirmPassword:profile.id
               }).save().then((user)=>{
                 console.log(JSON.stringify("New User created"))
               });
        }
   })
)
 

passport.use(
    new GithubStrategy({
 //Option for google strat
        clientID:keys.google.clientID,
        clientSecret:keys.google.clientSecret,
        callbackURL:'http://localhost:4040/api/auth/github/callback',
    },async (accessToken, refreshToken, profile,done) => {
    //passport callback funtion
    accessToken => {
        console.log(accessToken);
      }
 })
 );





module.exports=passport;