const app = require('../server');
//assert = require('assert'),
const chai = require('chai');
const assert = require('chai');
const expect = require('chai');
request = require('supertest');
//should = require('should');



describe("Authentication", () => {

    it("should respond with user is successfully logged", async () => {

        const result = await request(app).post("/login")
            .send({
              email: "kenley@gmail.com",
              password: "mypass1234",
            })
            .expect("Content-Type", /json/)
            .expect(200);

    });

    // it("should respond with session token", async () => {

    //     const result = await request(app).post("/login")
    //         .send({
    //             email: "kenley@gmail.com",
    //             password: "mypass1234",
    //         });

    //     expect(result.body.data).to.have.property(token);

    // });
});
   

describe("Register", () => {

  it("should respond with user is successfully registered", async () => {

      const result = await request(app).post("/register")
          .send({
            username: "Hollynios",
            email: "hollyn@gmail.com",
            password: "mypass1234",
            confirmPassword: "mypass1234",
          })
          .expect("Content-Type", /json/)
          .expect(200);

  });

});