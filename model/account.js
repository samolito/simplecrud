const mongoose = require('mongoose')


const AccountSchema = mongoose.Schema({
acn:{
    type:String,
    required: true
},
balance:{
    type:Number,
    required: true
},
currency:{
    type:String,
    required: true
},
// lastUpdated: { type: Date},
// created_at: { type: Date, default: Date.now },
user:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'User',
    
}
},{
    timestamps: true
});
AccountSchema.set('toJSON', { virtuals: true });

//Export model user with UserChema
module.exports = mongoose.model("Account", AccountSchema);