const mongoose = require('mongoose')


const UserSchema = mongoose.Schema({
fullName:{
    type:String,
    required: true
},
email:{
    type:String,
    required: true
},
phone:{
    type:String,
    required: true
},
password:{
    type:String,
    required: true
},

status:{
    type:Boolean,
    required: true,
    default:true
},
permissionLevel:{
    type:Number,
    default:1
},
accounts:[{
    type:mongoose.Schema.Types.ObjectId,
    ref:'Account',
    }]

// created_at: { type: Date, default: Date.now },

// lastUpdated: { type: Date},
});

UserSchema.set('toJSON', { virtuals: true });

//Export model user with UserChema
module.exports = mongoose.model("User", UserSchema);